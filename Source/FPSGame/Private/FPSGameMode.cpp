// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "FPSGameMode.h"
#include "FPSHUD.h"
#include "FPSCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Kismet/GameplayStatics.h"
#include "FPSGameState.h"
#include "Engine/World.h"

AFPSGameMode::AFPSGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/Blueprints/BP_Player"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AFPSHUD::StaticClass();

	//Future me, figure out why he does this.
	GameStateClass = AFPSGameState::StaticClass();
}

void AFPSGameMode::CompleteMission(APawn* Winner, bool bMissionSuccess)
{
	/*
	if (Winner)
	{
		Winner->DisableInput(nullptr); //if nullptr, calling pawn input disabled
	}*/

	if (Winner)
	{
		TArray<AActor*> ReturnedActors;

		UGameplayStatics::GetAllActorsOfClass(this, SpectatingViewpointClass, ReturnedActors);

		AActor* NewViewTarget = nullptr;
		if (ReturnedActors.Num() > 0)
		{
			NewViewTarget = ReturnedActors[0];
			APlayerController* PC = Cast<APlayerController>(Winner->GetController());
			for (FConstPlayerControllerIterator it = GetWorld()->GetPlayerControllerIterator(); it; it++)
			{
				APlayerController* PC = it->Get();
				if (PC)
				{
					PC->SetViewTargetWithBlend(NewViewTarget, 0.5f, EViewTargetBlendFunction::VTBlend_Cubic);
				}
			}

			AFPSGameState* GS = GetGameState<AFPSGameState>();
			if (GS)
			{
				GS->MulticastOnMissionCompleted(Winner, bMissionSuccess);
			}
			OnMissionCompleted(Winner, bMissionSuccess);
		}

		
	}

}
